from __future__ import absolute_import
from __future__ import print_function
from optparse import make_option

from typing import Any, Text

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandParser
from zerver.lib.actions import Realm, do_create_realm
from zerver.lib.domains import validate_domain

import re
import sys

class Command(BaseCommand):
    help = """Create a realm."""

    def add_arguments(self, parser):
        parser.add_argument('-s', '--string_id',
                            dest='string_id',
                            type=str,
                            help="A short name for the realm. If this "
                                 "installation uses subdomains, this will be "
                                 "used as the realm's subdomain.")

        parser.add_argument('-n', '--name',
                            dest='name',
                            type=str,
                            help='The user-visible name for the realm.')

    def handle(self, *args, **options):
        string_id = options["string_id"]
        name = options["name"]

        if string_id is None or name is None:
            print("\033[1;31mPlease provide a name and a string_id.\033[0m\n", file=sys.stderr)
            self.print_help("./manage.py", "create_realm")
            exit(1)

        realm = do_create_realm(string_id, name)
        print(string_id, "created.")

