# Ansible Role: Zulip

Setup [Zulip Server](https://zulipchat.com/) on Debian.

## Requirements

* Debian Stretch
* Zulip >2.0

## Role Variables

### General
* `zulip_version` - Version to download and install
* `zulip_external_host` - The user-accessible domain name for this Zulip server (EXTERNAL_HOST)
* `zulip_admin_mail` - Mail address of admin (ZULIP_ADMINISTRATOR)
* `zulip_local_uploads_dir` - Specify a symlink for uploads directory if you want to use another uploads location instead of the zulip home folder
* `zulip_gravatar_enabled` (bool) - Enable/Disable Gravatar
* `zulip_video_jitsi_url` - Specify Jitsi URL
* `zulip_configure_streams_enabled` (bool) - Include stream management task
* `zulip_file_links_enabled` (bool) - Parsing file links (ENABLE_FILE_LINKS)

### SSL
* `zulip_certbot_enabled` (bool) - Enable certbot at setup
* `zulip_self_signed_cert_enabled` (bool) - Use a self-signed certificate
* `zulip_ssl_cert_comm_enabled` (bool) - If you want to use a commercial certificate

### Mail
* `zulip_mail_outgoing_enabled` (bool) - Configure outgoing mail. See `defaults/main.yml` for additional settings.

### Auth
* `zulip_auth_backend_<BACKEND>_enable` (bool) - Configure authentication backends

#### LDAP
* `zulip_auth_ldap_uri` - LDAP Server
* `zulip_auth_ldap_mail_attr` - Specify ldap mail attribute if necessary
* `zulip_auth_ldap_user_attr`- Hash of additional ldap settings (AUTH_LDAP_USER_ATTR_MAP). See `defaults/main.yml` for additional settings.

### Mobile push notification
* `zulip_push_enabled` (bool) - Register with Zulip mobile push notification service

### Backup
* `zulip_backup_enabled` (bool) - Enable [Zulip backup](https://zulip.readthedocs.io/en/latest/production/maintain-secure-upgrade.html#backups)
* `zulip_backup_dir` - Path where backup files should be stored
* `zulip_backup_cron` (hash) - Configure cronjob for the backup
* `zulip_backup_cleanup_enabled` (bool) - If you want Ansible to remove older backups enable it and specify a specify the age value.


## Example Playbook

```yaml
- hosts: zulip

  roles:
    - role: zulip
      zulip_version: '2.0.2'
      zulip_external_host: 'chat.example.com'
      zulip_certbot_enabled: true
      ### mail
      zulip_mail_outgoing_enabled: true
      zulip_mail_use_tls: false
      zulip_mail_port: 25
      zulip_admin_mail: 'admin@example.com'
      zulip_mail_host: 'smtp.example.com'
```

### LDAP
Configure LDAP authentication

```yaml
- hosts: zulip

  roles:
    - role: zulip
      zulip_auth_backend_mail_enabled: false
      zulip_auth_backend_ldap_enabled: true
      zulip_auth_ldap_mail_attr: "\"mail\""
      zulip_auth_ldap_uri: 'ldap://ldap.example.com'
      # only allow members of zulip-users group to login
      zulip_auth_ldap_search: "\"OU=users,DC=example,DC=com\",ldap.SCOPE_SUBTREE, \"(&(memberOf=CN=zulip-users,OU=users,DC=example,DC=com)(uid=%(user)s))\""
      zulip_auth_ldap_bind_dn: 'CN=bind,OU=users,DC=example,DC=com'
      zulip_auth_ldap_bind_pw: 'test1234'
      # this will go into AUTH_LDAP_USER_ATTR_MAP
      zulip_auth_ldap_user_attr:
        full_name: "displayName" # use "displayName" attr instread of "cn"
        userAccountControl: "userAccountControl"
```

### Mobile Push
Register your Zulip server for push notifications. It will execute `manage.py register_server`

```yaml
- hosts: zulip

  roles:
    - role: zulip
      zulip_push_enabled: true
```

### Streams
This will create Streams via the [Zulip API](https://zulipchat.com/api/add-subscriptions) with a predefined bot - `zulip_api_bot`

```yaml
- hosts: zulip

  vars:
    streams:
      - name: marketing
        description: 'marketing team'
      - name: operations
        alias: 'ops'
        description: 'operations team and tactics'

  roles:
    - role: zulip
      zulip_configure_streams_enabled: true
      zulip_api_bot_password: 'test1234'
      zulip_streams: "{{ streams }}"
```
